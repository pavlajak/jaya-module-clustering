package model.code;

import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JavaParserTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class ClassTest {

    //should use @Before, but cannot import junit version now
    @Test
    void getName() {
        CombinedTypeSolver cts = new CombinedTypeSolver();
        cts.add(new ReflectionTypeSolver());
        cts.add(new JavaParserTypeSolver("."));
        ClassBuilder cb = new ClassBuilder();
        cb.setSymbolSolver(new JavaSymbolSolver(cts));
        cb.setJavaSrc(new File("src/main/java/analysis/Tokenizer.java"));
        Class c = cb.build();

        assertEquals("analysis.Tokenizer", c.getName());
    }

    @Test
    void countExternalCalls(){
        CombinedTypeSolver cts = new CombinedTypeSolver();
        cts.add(new ReflectionTypeSolver());
        cts.add(new JavaParserTypeSolver("."));
        ClassBuilder cb = new ClassBuilder();
        cb.setSymbolSolver(new JavaSymbolSolver(cts));
        cb.setJavaSrc(new File("src/main/java/analysis/Tokenizer.java"));
        Class c = cb.build();

        assertEquals(2, c.getExternalCalls().size());
    }

    @Test
    void getExternalCalls() {
        CombinedTypeSolver cts = new CombinedTypeSolver();
        cts.add(new ReflectionTypeSolver());
        cts.add(new JavaParserTypeSolver("."));
        ClassBuilder cb = new ClassBuilder();
        cb.setSymbolSolver(new JavaSymbolSolver(cts));
        cb.setJavaSrc(new File("src/main/java/analysis/Tokenizer.java"));
        Class c = cb.build();

        assertEquals("println()", c.getExternalCalls().get(0).getRef());
    }

    @Test
    void getExternalCallsDetail() {
        CombinedTypeSolver cts = new CombinedTypeSolver();
        cts.add(new ReflectionTypeSolver());
        cts.add(new JavaParserTypeSolver("."));
        ClassBuilder cb = new ClassBuilder();
        cb.setSymbolSolver(new JavaSymbolSolver(cts));
        cb.setJavaSrc(new File("src/main/java/analysis/Tokenizer.java"));
        Class c = cb.build();

        assertEquals("java.io.PrintStream", c.getExternalCalls().get(0).getTarget());
    }

    @Test
    void getCodeLayers(){
        CombinedTypeSolver cts = new CombinedTypeSolver();
        cts.add(new ReflectionTypeSolver());
        cts.add(new JavaParserTypeSolver("."));
        ClassBuilder cb = new ClassBuilder();
        cb.setSymbolSolver(new JavaSymbolSolver(cts));
        cb.setJavaSrc(new File("src/main/java/analysis/Tokenizer.java"));
        Class c = cb.build();

        assertEquals(2, c.getExternalCalls().get(0).getDepth().size());
        assertEquals(2, c.getExternalCalls().get(1).getDepth().size());
    }

    @Test
    void inspectCodeLayers(){
        CombinedTypeSolver cts = new CombinedTypeSolver();
        cts.add(new ReflectionTypeSolver());
        cts.add(new JavaParserTypeSolver("."));
        ClassBuilder cb = new ClassBuilder();
        cb.setSymbolSolver(new JavaSymbolSolver(cts));
        cb.setJavaSrc(new File("src/main/java/analysis/Tokenizer.java"));
        Class c = cb.build();

        assertEquals(CodeLayer.CATCH, c.getExternalCalls().get(0).getDepth().get(0));
        assertEquals(CodeLayer.TRY, c.getExternalCalls().get(0).getDepth().get(1));
    }
}