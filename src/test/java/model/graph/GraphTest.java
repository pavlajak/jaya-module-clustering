package model.graph;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class GraphTest {

    @Test
    void getArcs() {
        Node n = new Node();
        Node o = new Node();
        Arc a = new Arc(n, o, 1);
        n.addArc(a);
        Graph g = new Graph();
        g.addNode(n);
        assertEquals(1, g.getArcs().size());
        assertEquals(a, g.getArcs().iterator().next());
    }

    @Test
    void getNodeByLabel() {
        Node n = new Node("myNode");
        Node o = new Node("mySecondNode");
        Node p = new Node("myThirdNode");
        Graph g = new Graph();
        g.addNode(n);
        g.addNode(o);
        g.addNode(p);
        assertEquals(o, g.getNodeByLabel("mySecondNode"));
    }

    @Test
    void getNodes() {
        Set<Node> ns = new HashSet<Node>();
        ns.add(new Node("1"));
        ns.add(new Node("2"));
        ns.add(new Node("3"));

        Graph g = new Graph();
        ns.forEach(node -> g.addNode(node));

        assertTrue(ns.containsAll(g.getNodeSet()));
    }
}