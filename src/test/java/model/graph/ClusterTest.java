package model.graph;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ClusterTest {

    @Test
    void addNode() {
        Node n = new Node();
        Cluster c = new Cluster("");
        c.addNode(n);
        assertEquals(c, n.getCluster());
    }
}