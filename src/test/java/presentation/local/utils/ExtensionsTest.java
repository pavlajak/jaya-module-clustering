package presentation.local.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExtensionsTest {
    @Test
    public void jsonExt(){
        assertEquals("json", Extensions.getFileExtension("resources/sample_graph.json"));
    }

    @Test
    public void xmlExt(){
        assertEquals("xml", Extensions.getFileExtension("resources/sample_graph.xml"));
    }
}