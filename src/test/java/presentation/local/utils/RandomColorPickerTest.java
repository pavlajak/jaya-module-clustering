package presentation.local.utils;

import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class RandomColorPickerTest {

    @Test
    void randomColor() {
        Color c = RandomColorPicker.randomColor();
        Color c1 = RandomColorPicker.randomColor();
        assertNotEquals(c.toString(), c1.toString());
    }
}