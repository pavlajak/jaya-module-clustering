package presentation.local;

import model.graph.Node;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class EditListTest {

    @Test
    void current() {
        EditList el = new EditList();
        Set<Node> n = new HashSet<>();
        Edit e1 = new MoveEdit(n,1,1);
        el.push(e1);
        assertEquals(e1, el.current());;
    }

    @Test
    void undo() {
        EditList el = new EditList();
        Set<Node> n = new HashSet<>();
        Edit e1 = new MoveEdit(n,1,1);
        Edit e2 = new MoveEdit(n,2,2);
        el.push(e1);
        el.push(e2);
        el.undo();
        assertEquals(e1, el.current());
    }

    @Test
    void redo() {
        EditList el = new EditList();
        Set<Node> n = new HashSet<>();
        Edit e1 = new MoveEdit(n,1,1);
        Edit e2 = new MoveEdit(n,2,2);
        el.push(e1);
        el.push(e2);
        el.undo();
        el.redo();
        assertEquals(e2, el.current());
    }

    @Test
    void redoMany() {
        EditList el = new EditList();
        Set<Node> n = new HashSet<>();
        Edit e1 = new MoveEdit(n,1,1);
        Edit e2 = new MoveEdit(n,2,2);
        el.push(e1);
        el.push(e2);
        el.undo();
        el.redo();
        el.redo();
        el.redo();
        el.redo();
        assertEquals(e2, el.current());
    }

    @Test
    void undoMany() {
        EditList el = new EditList();
        Set<Node> n = new HashSet<>();
        Edit e1 = new MoveEdit(n,1,1);
        Edit e2 = new MoveEdit(n,2,2);
        el.push(e1);
        el.push(e2);
        el.undo();
        el.undo();
        el.undo();
        el.redo();
        assertEquals(e1, el.current());
    }

}