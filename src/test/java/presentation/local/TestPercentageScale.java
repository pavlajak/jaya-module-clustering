package presentation.local;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestPercentageScale {

    @Test
    public void scaleUpAndDown(){
        PercentageScale p = new PercentageScale();
        for (int i = 0; i < 10; i++) {
            p.incr();
        }
        for (int i = 0; i < 10; i++) {
            p.decr();
        }
        assertEquals(p.getValue(), 1, 0.00001);
    }

    @Test
    public void scaleDownAndUp(){
        PercentageScale p = new PercentageScale();
        for (int i = 0; i < 10; i++) {
            p.decr();
        }
        for (int i = 0; i < 10; i++) {
            p.incr();
        }
        assertEquals(p.getValue(), 1, 0.00001);
    }
}
