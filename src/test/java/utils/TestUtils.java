package utils;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestUtils {

    @Test
    public void testFindJsonFiles(){
        List<File> found = new ArrayList<>();
        File f = new File("./src/test/resources");
        Utils.rFindFiles(f, found, ".json");
        assertEquals(1, found.size());
    }

    @Test
    public void testFindXmlFiles(){
        List<File> found = new ArrayList<>();
        File f = new File("./src/test/resources");
        Utils.rFindFiles(f, found, ".xml");
        assertEquals(1, found.size());
    }
}
