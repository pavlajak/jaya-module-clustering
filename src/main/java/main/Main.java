package main;

import model.alg.MSJaya;
import model.code.ClassTree;
import model.code.ClassTreeBuilder;
import model.graph.Graph;
import org.xml.sax.SAXException;
import presentation.local.AppFrameMain;
import presentation.local.utils.Extensions;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    static Map<String, String> cli;

    /**
     * Program entrypoint.
     * @param args commands line arguments.
     */
    public static void main(String[] args) {
        if (args.length == 1 && args[0].equalsIgnoreCase("-g")){
            AppFrameMain afm = AppFrameMain.getInstance();
            afm.setVisible(true);
        } else if(args.length == 4 &&
                args[0].equalsIgnoreCase("-j") &&
                (args[2].equalsIgnoreCase("-xml") || args[2].equalsIgnoreCase("-json"))
        ){
            Graph g = new Graph();
            try {
                String graphExt = Extensions.getFileExtension(args[1]);
                if(graphExt.equalsIgnoreCase("xml")){
                    g.readFromXml(args[1]);
                } else if (graphExt.equalsIgnoreCase("json")){
                    g.readFromJson(args[1]);
                } else {
                    System.err.print("Problem with file: " + graphExt);
                }
                new MSJaya(g).apply();

                FileWriter fw = new FileWriter(new File(args[3]));
                if(args[2].equalsIgnoreCase("-xml")){
                    g.writeAsXml(fw);
                } else if(args[2].equalsIgnoreCase("-json")){
                    g.writeAsJson(fw);
                }

            } catch (IOException ex) {
                System.err.println(ex.getMessage());
            } catch (SAXException e) {
                System.err.println("Cannot parse " + args[1]);
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
        } else if(args.length == 6 &&
                args[0].equalsIgnoreCase("-r") &&
                args[2].equalsIgnoreCase("-c") &&
                (args[4].equalsIgnoreCase("-xml") || args[4].equalsIgnoreCase("-json"))
        ){
            List<File> classPath = new ArrayList<>();
            for(String s : args[3].split(";")){
                classPath.add(new File(s));
            }
            File srcRoot = new File(args[1]);

            Graph graph = new Graph();
            ClassTreeBuilder ctb = new ClassTreeBuilder();
            ctb.setJavaSrcRoot(srcRoot);
            for (File file : classPath) {
                ctb.addClassPath(file);
            }
            ClassTree ct = ctb.build();
            graph = ct.getGraph();

            try {
                FileWriter fw = new FileWriter(new File(args[5]));
                if(args[4].equalsIgnoreCase("-xml")) {
                    graph.writeAsXml(fw);
                } else if(args[4].equalsIgnoreCase("-json")){
                    graph.writeAsJson(fw);
                }
            } catch (IOException ex){
                System.err.println("Cannot write to " + args[5]);
            }
        } else {
            System.out.print(
                    "JMC usage options (choose one):\n" +
                            "\t-h | -help                                                   \tprint usage options\n" +
                            "\t-g                                                           \tlaunch the gui version\n" +
                            "\t-r <root>  -c <classpath-to-scan> -xml|json <save-to-file>   \tscan a java project, analyze and save\n" +
                            "\t-j <graph-file> -xml|json <save-to-file>                     \trun Jaya clustering on .json or .xml graph file\n" +
                            "Suggested usage: either launch the gui using '-g' or \n" +
                            "[1] run '-f root -c classpath-to-scan -xml|json file'\n" +
                            "[2] run '-jaya file' -xml|json file'\n"
            );
        }
    }

}