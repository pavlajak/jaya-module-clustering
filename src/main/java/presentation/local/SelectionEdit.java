package presentation.local;

import model.graph.Node;

import java.util.Set;

public class SelectionEdit extends Edit{

    Set<Node> selection;
    Set<Node> selected;
    boolean add;

    public SelectionEdit(Set<Node> exSel, Set<Node> newSel, boolean add){
        selection = exSel;
        selected = newSel;
        this.add = add;
    }

    @Override
    public void apply() {
        if(add){
            add();
        } else {
            remove();
        }
    }

    @Override
    public void undo() {
        if(add){
            remove();
        } else {
            add();
        }
    }

    private void remove(){
        selection.forEach(node -> {
            selection.remove(node);
        });
    }

    private void add(){
        selection.addAll(selected);
    }
}
