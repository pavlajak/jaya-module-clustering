package presentation.local;

/**
 * Class representing (view) scaling but on a rigid predefined scale.
 */
public class PercentageScale {

    private final double[] scaleValues = {.02, .05, .125, .25, .33, .50, .66, .75, .80, .90,
            1.00, 1.10, 1.25, 1.50, 1.75, 2.00, 2.50, 3.00, 4.00, 5.00, 750.00, 1000.00};
    private int pos;
    private double current;
    private final double FACTOR = 1.1;

    public PercentageScale(){
        //pos = 10;
        current = 1.0;
    }

    public double getValue(){
        return current;
        //return scaleValues[pos];
    }

    public void incr(){
        //if(pos < scaleValues.length - 1) pos++;
        current *= FACTOR;
    }

    public void decr(){
        //if(pos > 0) pos--;
        current /= FACTOR;
    }
}
