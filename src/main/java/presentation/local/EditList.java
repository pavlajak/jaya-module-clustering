package presentation.local;

import java.util.ArrayList;
import java.util.List;

public class EditList {
    List<Edit> list;
    int current;

    public EditList(){
        list = new ArrayList<>();
        current = -1;
    }

    public Edit current(){
        return list.get(current);
    }

    public void undo(){
        if(current == -1) return; //no edits to undo / beg. of the list
        current().undo();
        current--;
    }

    public void redo(){
        if(current == list.size() - 1) return; //no edits to redo / end of the list
        current++;
        current().apply();
    }

    public void push(Edit edit){
        list.subList(current + 1, list.size()).clear();
        list.add(edit);
        current++;
    }
}
