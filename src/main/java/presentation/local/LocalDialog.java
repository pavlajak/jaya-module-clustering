package presentation.local;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class LocalDialog extends JDialog{

    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField sourceRootTextField;
    private JButton changeSourceRoot;
    private JTextField classPathTextField;
    private JButton addClassPathButton;

    private boolean okClicked = false;

    public LocalDialog(JPanel cp) {
        super();
        setContentPane(cp);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        setPreferredSize(new Dimension(640, 180));
        setSize(640, 180);

        String java_home = System.getenv("JAVA_HOME");
        if (!java_home.isEmpty()) {
            if (!java_home.endsWith(";")) {
                classPathTextField.setText(java_home + ";");
            } else {
                classPathTextField.setText(java_home);
            }
        }

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        changeSourceRoot.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onChangeSourceRoot();
            }
        });

        addClassPathButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onAddClassPath();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);


    }

    private void onChangeSourceRoot() {
        // add your code here
        JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        int returnValue = fc.showOpenDialog(null);
        String resp = null;
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            sourceRootTextField.setText(fc.getSelectedFile().getAbsolutePath());
        }
    }

    private void onAddClassPath() {
        // add your code here
        JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        int returnValue = fc.showOpenDialog(null);
        String resp = null;
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            classPathTextField.setText(classPathTextField.getText() + fc.getSelectedFile().getAbsolutePath() + ";");
        }
    }

    private void onOK() {
        // add your code here
        okClicked = true;
        dispose();

    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public String getSourceRoot() {
        return sourceRootTextField.getText();
    }

    public String getClassPath() {
        return classPathTextField.getText();
    }

    public boolean getOkCliced() {
        return okClicked;
    }
}
