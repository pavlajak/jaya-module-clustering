package presentation.local;

public abstract class Edit {

    public abstract void apply();
    public abstract void undo();
}
