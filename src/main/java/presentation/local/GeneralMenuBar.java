package presentation.local;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GeneralMenuBar extends JMenuBar {
    public GeneralMenuBar(){
        super();
        JMenu menuFile = new JMenu("File");
        JMenu menuHelp = new JMenu("Help");
        add(menuFile);
        add(menuHelp);

        JMenuItem menuFileOpen = new JMenuItem("Open");
        JMenuItem menuFileSaveAs = new JMenuItem("Save as");
        menuFile.add(menuFileOpen);
        menuFile.add(menuFileSaveAs);

        JMenuItem menuHelpHelp = new JMenuItem("Help");
        menuHelp.add(menuHelpHelp);
        menuHelpHelp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(menuHelp, "Version 0.1");
            }
        });
    }
}
