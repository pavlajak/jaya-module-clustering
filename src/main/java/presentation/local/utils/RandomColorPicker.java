package presentation.local.utils;

import java.awt.*;
import java.util.Random;

public class RandomColorPicker {
    static Random r = new Random();

    public static Color randomColor(){
        Color col = new Color(r.nextFloat(), r.nextFloat(), r.nextFloat());
        return col;
    }
}
