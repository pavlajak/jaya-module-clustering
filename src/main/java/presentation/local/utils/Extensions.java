package presentation.local.utils;

import java.io.File;

public class Extensions {

    public static String getFileExtension(String path){
        File f = new File(path);
        if(f == null){
            return "file does not exist" + path;
        }
        String name = f.getName();
        return name.substring(name.lastIndexOf('.') + 1);
    }
}
