package presentation.local.layout;

import model.graph.Cluster;
import model.graph.Graph;
import presentation.local.utils.Coord;

import java.util.*;

public class ClusterRandomLayout extends RandomLayout{

    public ClusterRandomLayout(Graph g, int w, int h) {
        super(g, w, h);
    }

    public ClusterRandomLayout(Graph g, int w, int h, Random rand) {
        super(g, w, h, rand);
        r = rand;
    }

    @Override
    public void apply() {
        List<Cluster> clusters = new ArrayList<>(graph.getClusters());
        if(clusters == null || clusters.isEmpty()){
            new RandomLayout(graph, width, height, r).apply();
        } else {
            int p = 1 + area / graph.getClusters().size();
            int q = 1 + (int) Math.sqrt(p);
            Map<Cluster, Coord> coords = new HashMap<>();

            int curCluster = 0;
            for(Cluster cluster : clusters){

                coords.put(cluster, new Coord(r.nextInt(width) - width/2, r.nextInt(height) - height/2));
                curCluster++;
            }
            clusters.forEach(cluster -> {
                cluster.getNodes().forEach(node -> {
                    int newX = (int) coords.get(cluster).x() - (int) (r.nextDouble() * q - .5 * q);
                    int newY = (int) coords.get(cluster).y() - (int) (r.nextDouble() * q - .5 * q);

                    node.setX(newX);
                    node.setY(newY);
                });
            });
        }
    }
}
