package presentation.local.layout;

import model.graph.Arc;
import model.graph.Graph;
import model.graph.Node;

import java.util.*;

public class ForceBasedLayout extends BasicLayout{
    public final int ITERATIONS; // number of iterations for the algorithm to do

    public ForceBasedLayout(Graph g, int w, int h) {
        super(g, w, h);
        ITERATIONS = 4096;
    }

    public ForceBasedLayout(Graph g, int w, int h, int iter) {
        super(g, w, h);
        ITERATIONS = iter;
    }

    private static final float SPEED_DIVISOR = 8000;
    private static final float AREA_MULTIPLICATOR = 1000;

    /**
     * Applies a force-based layout. Nodes are forced apart if they are not connected by an arc. If two nodes are
     * connected by an arc, they are moved closer to each other.
     */
    @Override
    public void apply(){
        List<Node> nodes = new ArrayList<>(graph.getNodes().values());
        Set<Arc> arcs = graph.getArcs();


        double gravity = 100;
        double speed = 10;
        for (int i = 0; i < ITERATIONS; i++) {
            if(i % (ITERATIONS / 10) == 0){
                System.out.println("Working...");
            }
            for (Node node : nodes) {
                node.xDispl = 0;
                node.yDispl = 0;
            }

            double maxDisplace = Math.sqrt(AREA_MULTIPLICATOR * area) / 5f;
            float k = (float) Math.sqrt((AREA_MULTIPLICATOR * area) / (1f + nodes.size()));

            for (Node node1 : nodes) {
                for (Node node2 : nodes) {
                    if (node1 != node2) {
                        double xDist = node1.x() - node2.x();
                        double yDist = node1.y() - node2.y();
                        double dist = Math.sqrt(xDist * xDist + yDist * yDist);

                        if (dist > 0) {
                            double repForce = k * k / dist;
                            //System.out.printf("rep force [%f, %f]\n", xDist / dist * repForce, yDist / dist * repForce);
                            node1.xDispl += xDist / dist * repForce;
                            node1.yDispl += yDist / dist * repForce;
                        }
                    }
                }
            }

            for (Arc arc : arcs) {
                Node from = arc.getFrom();
                Node to = arc.getTo();

                double xDist = from.x() - to.x();
                double yDist = from.y() - to.y();
                double dist = Math.sqrt(xDist * xDist + yDist * yDist);

                if (dist > 0) {
                    double attForce = dist * dist / k;
                    //System.out.printf("att force [%f, %f]\n", xDist / dist * attForce, yDist / dist * attForce);
                    from.xDispl -= 10000 * (xDist / dist * attForce);
                    from.yDispl -= 10000 * (yDist / dist * attForce);
                    to.xDispl += 10000 * (xDist / dist * attForce);
                    to.yDispl += 10000 * (yDist / dist * attForce);
                }
            }

            //gravity and speed
            for (Node node : nodes) {
                double d = Math.sqrt(node.x() * node.x() + node.y() * node.y());
                double gf = 0.01f * k * gravity * d;
                node.xDispl -= gf * node.x() / d;
                node.yDispl -= gf * node.y() / d;
            }

            for (Node node : nodes) {
                node.xDispl *= speed / SPEED_DIVISOR;
                node.yDispl *= speed / SPEED_DIVISOR;
            }

            for (Node node : nodes) {
                double xDist = node.xDispl;
                double yDist = node.yDispl;
                double dist = Math.sqrt(node.xDispl * node.xDispl + node.yDispl * node.yDispl);
                if (dist > 0) {
                    double limitedDist = Math.min(maxDisplace * (speed / SPEED_DIVISOR), dist);
                    //double limitedDist = Math.min(maxDisplace, dist);
                    node.setX(node.x() + xDist / dist * limitedDist);
                    node.setY(node.y() + yDist / dist * limitedDist);
                }
            }
        }
        System.out.println("Done!");

    }
}
