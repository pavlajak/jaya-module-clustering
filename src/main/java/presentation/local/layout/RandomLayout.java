package presentation.local.layout;

import model.graph.Graph;
import java.util.Random;

public class RandomLayout extends GraphLayout{
    protected Random r;

    public RandomLayout(Graph g, int w, int h){
        super(g, w, h);
        r = new Random();
    }

    public RandomLayout(Graph g, int w, int h, Random rand){
        super(g, w, h);
        r = rand;
    }

    @Override
    public void apply(){
        graph.getNodes().forEach((s, node) -> {
            node.setX((r.nextDouble() - .5) * width);
            node.setY((r.nextDouble() - .5) * height);
        });
    }
}
