package presentation.local.layout;

import model.graph.Graph;
import model.graph.Node;

import java.util.Collection;

public abstract class GraphLayout {
    protected Graph graph;
    protected int width, height, area;


    /**
     * Applies the layout
     */
    public abstract void apply();

    /**
     * Scales this graph so that every node's x and y coordinates are within the range 0-w and 0-h respectively.
     */
    public void scaleBack(){
        //rescale back to view
        Collection<Node> nodes = graph.getNodes().values();

        // find minimimums and shift to 0
        double minX = Double.MAX_VALUE;
        double minY = Double.MAX_VALUE;
        for (Node node: nodes) {
            minX = Math.min(minX, node.x() - node.getSize());
            minY = Math.min(minY, node.y() - node.getSize());
        }
        for (Node node: nodes) {
            node.setX(node.x() - minX);
            node.setY(node.y() - minY);
        }

        //find maximums and scale to view border
        double maxX = Double.MIN_VALUE;
        double maxY = Double.MIN_VALUE;
        for (Node node: nodes) {
            maxX = Math.max(maxX, node.x() + node.getSize());
            maxY = Math.max(maxY, node.y() + node.getSize());
        }
        double widthScale = maxX / width;
        double heightScale = maxY / height;
        for (Node node: nodes) {
            node.setX(  node.x() / widthScale);
            node.setY(  node.y() / heightScale);
        }
    }

    /**
     * @param g Graph this layout will apply onto.
     * @param w Desired width bound. Calling scale() will ensure every node will have the x coordinate between 0 and @w.
     * @param h Desired height bound. Calling scale() will ensure every node will have the y coordinate between 0 and @h.
     */
    public GraphLayout(Graph g, int w, int h){
        graph = g;
        width = w;
        height = h;
        area = width * height;
    }
}
