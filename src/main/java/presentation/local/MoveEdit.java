package presentation.local;

import model.graph.Node;

import java.util.Set;

public class MoveEdit extends Edit{

    private Set<Node> nodes;
    double dx, dy;

    public MoveEdit(Set<Node> nodes, double dx, double dy){
        this.nodes = nodes;
        this.dx = dx;
        this.dy = dy;
    }

    public void apply(){
        nodes.forEach(node -> {
            node.setX(node.x() + dx);
            node.setY(node.y() + dy);
        });
    }

    public void undo(){
        nodes.forEach(node -> {
            node.setX(node.x() - dx);
            node.setY(node.y() - dy);
        });
    }

}