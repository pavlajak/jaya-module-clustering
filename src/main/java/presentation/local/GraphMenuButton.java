package presentation.local;

import javax.swing.*;
import java.awt.*;

public class GraphMenuButton extends JButton {
    public final String DESCRIPTION;
    public final int PREFERRED_ICON_SIZE = 20;

    public GraphMenuButton(String description, String filename){
        super(new ImageIcon(filename));
        DESCRIPTION = description;
        this.setPreferredSize(new Dimension(this.getIcon().getIconWidth(), this.getIcon().getIconHeight()));
        //this.setBackground(Color.YELLOW);
    }
}
