package presentation.local;

import model.alg.Clustering;
import model.alg.MSJaya;
import model.code.ClassTree;
import model.code.ClassTreeBuilder;
import model.graph.Graph;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AppFrameMain extends JFrame implements ComponentListener {

        public static final String FRAME_TITLE = "JMC - Java Module Clustering";

        private static AppFrameMain instance;

        public static AppFrameMain getInstance(){
                if(instance == null){
                        instance = new AppFrameMain();
                }
                return instance;
        }

        private GraphicsDevice gd;
        private int screenWidth;
        private int screenHeight;
        private int frameWidth;
        private int frameHeight;

        private JMenuBar jmb;
        JMenu menuFile;
        JMenu menuEdit;
        JMenu menuHelp;
        JMenuItem menuFileOpen;
        JMenu menuFileSaveAs;
        JMenuItem menuFileSaveAsXml;
        JMenuItem menuFileSaveAsJson;
        JMenuItem menuEditUndo;
        JMenuItem menuEditRedo;
        JMenuItem menuEditLayoutRand;
        JMenuItem menuEditLayoutForce;
        JMenuItem menuEditClustering;
        JMenuItem menuHelpHelp;
        JMenu menuFileCreateMDG;
        //JMenuItem menuFileCreateMDGGit;
        JMenuItem menuFileCreateMDGLocal;
        private GraphDrawPanel gdp;

        private Graph graph;

        public AppFrameMain(){
                super("JMC");

                gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
                screenWidth = gd.getDisplayMode().getWidth();
                screenHeight = gd.getDisplayMode().getHeight();
                frameWidth = 1280;
                frameHeight = 720;

                setLocation( (screenWidth - frameWidth) / 2, (screenHeight - frameHeight) / 2);
                setPreferredSize(new Dimension(frameWidth, frameHeight));
                gdp = new GraphDrawPanel();
                add(gdp);
                setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                pack();
                setLocationRelativeTo(null);

                MenuBarHandler mbh = new MenuBarHandler();
                jmb = new JMenuBar();
                menuFile = new JMenu("File");
                menuEdit = new JMenu("Edit");
                menuHelp = new JMenu("Help");
                jmb.add(menuFile);
                jmb.add(menuHelp);
                jmb.add(menuEdit);

                menuFileOpen = new JMenuItem("Open");
                menuFileOpen.addActionListener(mbh);
                menuFileSaveAs = new JMenu("Save as");
                menuFileSaveAs.addActionListener(mbh);
                menuFileSaveAsXml = new JMenuItem("Save as XML");
                menuFileSaveAsXml.addActionListener(mbh);
                menuFileSaveAsJson = new JMenuItem("Save as JSON");
                menuFileSaveAsJson.addActionListener(mbh);
                menuFileCreateMDG = new JMenu("Create module dependency graph");
                menuFileCreateMDG.addActionListener(mbh);
                //menuFileCreateMDGGit = new JMenuItem("From git");
                //menuFileCreateMDGGit.addActionListener(mbh);
                menuFileCreateMDGLocal = new JMenuItem("From local sources");
                menuFileCreateMDGLocal.addActionListener(mbh);

                menuFileSaveAs.add(menuFileSaveAsXml);
                menuFileSaveAs.add(menuFileSaveAsJson);
                //menuFileCreateMDG.add(menuFileCreateMDGGit);
                menuFileCreateMDG.add(menuFileCreateMDGLocal);
                menuFile.add(menuFileOpen);
                menuFile.add(menuFileSaveAs);
                menuFile.add(menuFileCreateMDG);

                menuEditClustering = new JMenuItem("Create clustering");
                menuEditClustering.addActionListener(mbh);
                menuEditUndo = new JMenuItem("Undo \t(ctrl+z)");
                menuEditUndo.addActionListener(mbh);
                menuEditRedo = new JMenuItem("Redo \t(ctrl+shit+z)");
                menuEditRedo.addActionListener(mbh);

                menuEditLayoutRand = new JMenuItem("Apply random layout");
                menuEditLayoutRand.addActionListener(mbh);
                menuEditLayoutForce = new JMenuItem("Apply force-directed layout");
                menuEditLayoutForce.addActionListener(mbh);

                menuEdit.add(menuEditClustering);
                menuEdit.add(menuEditUndo);
                menuEdit.add(menuEditRedo);
                menuEdit.add(menuEditLayoutRand);
                menuEdit.add(menuEditLayoutForce);

                menuHelpHelp = new JMenuItem("Help");
                menuHelpHelp.addActionListener(mbh);
                menuHelp.add(menuHelpHelp);

                setJMenuBar(jmb);

                addComponentListener(this);
        }

        @Override
        public void componentResized(ComponentEvent e) {

        }

        @Override
        public void componentMoved(ComponentEvent e) {

        }

        @Override
        public void componentShown(ComponentEvent e) {
                gdp.refreshBounds();
        }

        @Override
        public void componentHidden(ComponentEvent e) {

        }

        /**
         * Listener for the the frame menu bar.
         */
        private class MenuBarHandler implements ActionListener {

                @Override
                public void actionPerformed(ActionEvent e) {
                        JFileChooser fc;
                        int returnValue;
                        String resp;
                        if (e.getSource()== menuFileOpen) {
                                fc = new JFileChooser();
                                returnValue = fc.showOpenDialog(null);
                                if (returnValue == JFileChooser.APPROVE_OPTION) {
                                        resp = open(fc.getSelectedFile());
                                        JOptionPane.showMessageDialog(menuFileOpen, resp);
                                }
                        } else if(e.getSource() == menuFileSaveAsXml){
                                fc = new JFileChooser();
                                returnValue = fc.showSaveDialog(null);
                                if(returnValue == JFileChooser.APPROVE_OPTION){
                                        resp = saveAsXml(fc.getSelectedFile());
                                        JOptionPane.showMessageDialog(menuFileSaveAsXml, resp);
                                }
                        } else if(e.getSource() == menuFileSaveAsJson){
                                fc = new JFileChooser();
                                returnValue = fc.showSaveDialog(null);
                                if(returnValue == JFileChooser.APPROVE_OPTION){
                                        resp = saveAsJson(fc.getSelectedFile());
                                        JOptionPane.showMessageDialog(menuFileSaveAsJson, resp);
                                }
                        } else if(e.getSource() == menuFileCreateMDGLocal){

                                JTextField srcRoot = new JTextField();
                                JTextField classPath = new JTextField();
                                String java_home = System.getenv("JAVA_HOME");

                                final JComponent[] inputs = new JComponent[] {
                                        new JLabel("Source code root (should point to .../src/main/->java<-)"),
                                        srcRoot,
                                        new JLabel("Class path (separated by ;)"),
                                        classPath
                                };
                                if (!java_home.isEmpty()) {
                                        if (!java_home.endsWith(";")) {
                                                classPath.setText(java_home + ";");
                                        } else {
                                                classPath.setText(java_home);
                                        }
                                }
                                int result = JOptionPane.showConfirmDialog(null, inputs, "Create from local source", JOptionPane.PLAIN_MESSAGE);
                                if (result == JOptionPane.OK_OPTION) {
                                        List<File> cpFiles = new ArrayList<>();
                                        for(String s : classPath.getText().split(";")){
                                                cpFiles.add(new File(s));
                                        }
                                        createFromLocal(new File(srcRoot.getText()), cpFiles);
                                }
                        } else if(e.getSource() == menuEditClustering){
                                findClusteringJaya();
                                redrawGraph();
                        } else if(e.getSource() == menuEditUndo){
                                gdp.undo();
                        } else if(e.getSource() == menuEditRedo){
                                gdp.redo();
                        } else if(e.getSource() == menuEditLayoutRand){
                                gdp.randLayout();
                        } else if(e.getSource() == menuEditLayoutForce){
                                gdp.forceLayout();
                        } else if(e.getSource()== menuHelpHelp){
                                JOptionPane.showMessageDialog(menuHelpHelp, "JMC Version 0.1");
                        } else {
                                System.out.println("Action Handler not ready for this event: " + e);
                        }
                        repaint();
                }
        }

        /**
         * Called by the File -> Open menu item. Infers the file contents schema by file extension. Supported extensions are xml and json.
         * @param f File to be opened.
         * @return Message to be displayed to user based on parsing success.
         */
        private String open(File f){
                String fileName = f.getName();
                graph = new Graph(f.getName());
                String retMessage = null;

                if(fileName.substring(fileName.length() - 3).equalsIgnoreCase("xml")){
                        try {
                                graph.readFromXml(f.getAbsolutePath());
                                retMessage = "XML Graph successfully read.";
                        } catch (Exception e) {
                                return e.getMessage();
                        }
                } else if(fileName.substring(fileName.length() - 4).equalsIgnoreCase("json")){
                        try {
                                graph.readFromJson(f.getAbsolutePath());
                                retMessage = "JSON Graph successfully read.";
                        } catch (FileNotFoundException e) {
                                return e.getMessage();
                        }
                }
                if(retMessage == null){
                        retMessage = "Graph could not be read. Ensure the file extension is either " +
                                ".json or .xml depending on the file format.";
                }
                gdp.setGraph(graph);
                setTitle(FRAME_TITLE + " [" + f.getAbsolutePath() + "]");
                return retMessage;
        }

        private String saveAsXml(File f){
                try {
                        FileWriter fw = new FileWriter(f);
                        graph.writeAsXml(fw);
                        fw.close();
                } catch (IOException e) {
                        return "Something went wrong! " + e.getMessage();
                }
                return "File saved.";
        }

        private String saveAsJson(File f){
                try {
                        FileWriter fw = new FileWriter(f);
                        graph.writeAsJson(fw);
                        fw.close();
                } catch (IOException e) {
                        return "Something went wrong! " + e.getMessage();
                }
                return "File saved.";
        }

        private String createFromLocal(File srcRoot, List<File> classPath){
                ClassTreeBuilder ctb = new ClassTreeBuilder();
                ctb.setJavaSrcRoot(srcRoot);
                for (File file : classPath) {
                        ctb.addClassPath(file);
                }
                ClassTree ct = ctb.build();
                graph = ct.getGraph();
                gdp.setGraph(graph);
                setTitle(FRAME_TITLE + " [" + srcRoot + "]");
                return "Built graph from " + srcRoot;
        }

        public void debugOpen(File f){
                open(f);
        }

        public void debugCreateFromLocal(String srcRoot, String classPath){
                String[] cps = classPath.split(";");
                List<File> cpl = new ArrayList<>();
                for (String cp : cps) {
                        cpl.add(new File(cp));
                }
                createFromLocal(new File(srcRoot), cpl);
        }

        public void findClusteringJaya(){
                if(graph == null) return;
                Clustering jaya = new MSJaya(graph);
                jaya.apply();
                gdp.setColors();
        }

        public void redrawGraph(){
                gdp.repaint();
                repaint();
        }
}
