package presentation.local;

import model.graph.Arc;
import model.graph.Cluster;
import model.graph.Graph;
import model.graph.Node;
import presentation.local.layout.ClusterRandomLayout;
import presentation.local.layout.ForceBasedLayout;
import presentation.local.utils.RandomColorPicker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Ellipse2D;
import java.util.*;

public class GraphDrawPanel extends JPanel {

    private Graph graph;
    private PercentageScale scale;
    private double relativeX, relativeY;
    private Set<Node> selectedNodes;
    private Arc selectedArc;
    private Rectangle selectedArea;
    private Map<Cluster, Color> colors;

    private EditList editStack;

    final static float[] dash1 = { 10.0f };
    final static BasicStroke dashed = new BasicStroke(1.0f,
            BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash1, 0.0f);
    public static Font defaultFont = new Font("Calibri", Font.PLAIN, 12);

    public GraphDrawPanel(){
        super();
        scale = new PercentageScale();
        selectedNodes = new HashSet<>();
        colors = new HashMap<>();
        editStack = new EditList();
        colors.put(null, Color.GRAY);

        Rectangle bounds = getBounds();
        relativeX = bounds.x / 2;
        relativeY = bounds.y / 2;

        MouseWheelHandler mwh = new MouseWheelHandler();
        addMouseWheelListener(mwh);
        CustomMouseListener cml = new CustomMouseListener();
        addMouseListener(cml);
        addMouseMotionListener(cml);

        getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_DOWN_MASK), "ctrl+z");
        getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_Z,
                InputEvent.CTRL_DOWN_MASK | InputEvent.SHIFT_DOWN_MASK), "ctrl+shift+z");
        getActionMap().put("ctrl+z", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                undo();
                repaint();
            }
        });
        getActionMap().put("ctrl+shift+z", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                redo();
                repaint();
            }
        });
    }

    public void undo(){
        editStack.undo();
    }

    public void redo(){
        editStack.redo();
    }

    /**
     *  This method draws a node onto the panel. Takes into account current view displacement and scale.
     * @param g passed down graphics components.
     * @param n node from model to be drawn. Visualization values should be initialized (by using one of the layout methods)
     *          before calling this method.
     */
    private void drawNode(Graphics g, Node n){
        double rescale = scale.getValue();
        //the following variables are rescaled
        int radius = (int) (n.getSize() * rescale / 2);
        int cx = (int) ((n.x() - relativeX) * rescale); //node center X coord
        int cy = (int) ((n.y() - relativeY) * rescale); //node center Y coord
        int tlx = cx - radius; //top left corner X
        int tly = cy - radius; //top left corner Y

        Graphics2D g2d = (Graphics2D) g;
        Ellipse2D.Double circle = new Ellipse2D.Double(tlx, tly, radius * 2, radius * 2);

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        g2d.setColor(colors.get(n.getCluster()));

        if(selectedNodes != null && selectedNodes.contains(n)){
            g2d.setColor(Color.LIGHT_GRAY);
            g2d.drawRect(tlx, tly, radius * 2, radius * 2);
        }
        g2d.fill(circle);

        g2d.setColor(Color.BLACK);
        g2d.setFont(defaultFont);
        g2d.drawString(
                n.getLabel(),
                cx - (g2d.getFontMetrics(defaultFont).stringWidth(n.getLabel()) / 2),
                cy + (defaultFont.getSize() / 2)
        );
    }

    /**
     * This method draws all the arcs going from node @n.
     * @param g passed down graphics component.
     * @param n Node whose arcs should be drawn.
     */
    private void drawNodeArcs(Graphics g, Node n){
        Set<Arc> arcs = n.getArcs();

        Graphics2D g2d = (Graphics2D)g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        double rescale = scale.getValue();
        int radius1 = (int) (n.getSize() * rescale / 2);
        int cx1 = (int) ((n.x() - relativeX) * rescale); //node center X coord
        int cy1 = (int) ((n.y() - relativeY) * rescale); //node center Y coord

        for (Arc arc: arcs) {
            Node to = arc.getTo();

            int radius2 = (int) (to.getSize() * rescale / 2);
            int cx2 = (int) ((to.x() - relativeX) * rescale); //node center X coord
            int cy2 = (int) ((to.y() - relativeY) * rescale); //node center Y coord

            double dx = cx1 - cx2;
            double dy = cy1 - cy2;
            double l = Math.sqrt(dx * dx + dy * dy);
            double r1l = radius1 / l;
            double r2l = radius2 / l;
            double newX = cx2 + dx * r2l;
            double newY = cy2 + dy * r2l;

            g2d.drawLine(cx1, cy1, (int) newX, (int) newY);

            dx /= l * 0.15;
            dy /= l * 0.15;

            g2d.fillPolygon(new int[]{(int)newX, (int) (newX + dx + (dy / 2)), (int) (newX + dx - (dy / 2))},
                    new int[]{(int)newY, (int) (newY + dy - (dx / 2)), (int) (newY + dy + (dx / 2))},
                    3);

        }
    }

    public void randLayout(){
        if(graph == null) return;
        new ClusterRandomLayout(graph, getWidth(), getHeight()).apply();
    }

    public void forceLayout(){
        if(graph == null) return;
        new ForceBasedLayout(graph, getWidth(), getHeight()).apply();
    }

    /**
     * This method finds a Node that occupies the pixel at @x, @y.
     * @param x coordinate.
     * @param y coordinate.
     * @return Reference to the node at @x, @y.
     */
    private Node getNodeByCoord(int x, int y){
        if(graph == null) return null;
        for (Node node: graph.getNodes().values()) {
            //this code section repeats and could be computed only once per repaint() call
            double rescale = scale.getValue();
            int radius = (int) (node.getSize() * rescale / 2);
            int cx = (int) ((node.x() - relativeX) * rescale); //node center X coord
            int cy = (int) ((node.y() - relativeY) * rescale); //node center Y coord
            if((cx - x) * (cx - x) + (cy - y) * (cy - y) < radius * radius){
                return node;
            }
        }
        return null;
    }

    /**
     * This method finds a Node that occupies the pixel at @x, @y.
     * @param x coordinate.
     * @param y coordinate.
     * @return Reference to the node at @x, @y.
     */
    private Set<Node> getNodesInRect(int x, int y, int width, int height){
        if(graph == null) return null;
        Set<Node> toReturn = new HashSet<>();
        for (Node node: graph.getNodes().values()) {
            double rescale = scale.getValue();
            int cx = (int) ((node.x() - relativeX) * rescale); //node center X coord
            int cy = (int) ((node.y() - relativeY) * rescale); //node center Y coord
            if(cx >= x && cx <= x + width &&
                cy >= y && cy <= y + height){
                toReturn.add(node);
            }
        }
        return toReturn;
    }

    /**
     * Class for right-click menu. Has menu items for adding nodes and arcs, centering the view,
     */
    private class EditPopupMenu extends JPopupMenu{ //TODO! remove?
        JMenuItem editNode;
        JMenuItem addNode;
        JMenuItem addArc;
        JMenuItem centerView;
        JMenuItem findNode;

        public EditPopupMenu() {

            editNode = new JMenuItem("Edit node");
            add(editNode);
            addArc = new JMenuItem("Add arc");
            add(addArc);

            if(selectedNodes.size() != 1){
                editNode.setEnabled(false);
                addArc.setEnabled(false);
            }

            addNode = new JMenuItem("Add node");
            add(addNode);
            centerView = new JMenuItem("Center view");
            add(centerView);
            findNode = new JMenuItem("Find node");
            add(findNode);
        }
    }

    /**
     * Listener that handles click & drag feature that moves the view.
     * ALso handles selecting and moving pane contents.
     */
    private class CustomMouseListener extends MouseAdapter {
        private Point clicked;

        @Override
        public void mousePressed(MouseEvent e){
            clicked = e.getPoint();
        }

        @Override
        public void mouseClicked(MouseEvent e){
            if(SwingUtilities.isRightMouseButton(e)){
                //EditPopupMenu menu = new EditPopupMenu();
                //menu.show(e.getComponent(), e.getX(), e.getY());
            } else if (SwingUtilities.isLeftMouseButton(e)){
                Node toAdd = getNodeByCoord(e.getX(), e.getY());
                if((e.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) != InputEvent.CTRL_DOWN_MASK){
                    selectedNodes.clear();
                }
                if(toAdd != null){
                    selectedNodes.add(toAdd);
                }
            }
            repaint();
        }

        @Override
        public void mouseReleased(MouseEvent e){
            if(SwingUtilities.isRightMouseButton(e)){
                if(selectedArea != null){
                    selectedNodes = getNodesInRect(selectedArea.x, selectedArea.y, selectedArea.width, selectedArea.height);
                    selectedArea = null;
                }
            } else if(SwingUtilities.isLeftMouseButton(e)){
                if (selectedNodes.size() != 0 && xEditDrag != 0 && yEditDrag != 0) {
                    editStack.push(new MoveEdit(new HashSet<>(selectedNodes), xEditDrag, yEditDrag));
                    editDrag = false;
                    xEditDrag = 0;
                    yEditDrag = 0;
                }
            }
            repaint();
        }

        private boolean editDrag = false;
        private double xEditDrag = 0;
        private double yEditDrag = 0;

        @Override
        public void mouseDragged(MouseEvent e){
            Point draggedTo = e.getPoint();
            int deltaX = draggedTo.x - clicked.x;
            int deltaY = draggedTo.y - clicked.y;
            if(SwingUtilities.isLeftMouseButton(e)){
                if(selectedNodes.isEmpty()){ //drag view
                    relativeX -=  deltaX / scale.getValue();
                    relativeY -= deltaY / scale.getValue();
                } else { //drag node
                    selectedNodes.forEach(node -> {
                        node.setX(node.x() + (deltaX / scale.getValue()));
                        node.setY(node.y() + (deltaY / scale.getValue()));
                        //editStack.push(new Edit(
                        //                new HashSet<Node>(selectedNodes),
                        //                (deltaX / scale.getValue()),
                        //                (deltaY / scale.getValue())));
                    });
                    editDrag = true;
                    xEditDrag += deltaX / scale.getValue();
                    yEditDrag += deltaY / scale.getValue();
                }
                clicked = draggedTo;
            } else if (SwingUtilities.isRightMouseButton(e)){ //rectangle selection here perhaps?
                selectedArea = new Rectangle(clicked.x, clicked.y, deltaX, deltaY);
            }
            repaint();
        }
    }

    /**
     * Listener that handles view scaling.
     */
    private class MouseWheelHandler implements MouseWheelListener {

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            if (e.isControlDown()) {
                if (e.getWheelRotation() < 0) {
                    scale.incr();
                    repaint();
                } else {
                    scale.decr();
                    repaint();
                }
            } else {
                getParent().dispatchEvent(e);
            }
        }
    }

    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);

        if(graph != null){
            graph.getNodes().forEach(((s, node) -> {
                drawNodeArcs(g, node);
            }));
            graph.getNodes().forEach(((s, node) -> {
                drawNode(g, node);
            }));
        }

        g.setColor(Color.BLACK);
        g.setFont(defaultFont);
        g.drawRect(getBounds().width - 50, 10, 40, 20);
        g.drawString(String.format("%.1f%%", scale.getValue() * 100), getBounds().width - 48, 24);

        Graphics2D g2d = (Graphics2D) g;
        if(selectedArea != null){
            g2d.setStroke(dashed);
            g2d.draw(selectedArea);
        }
    }

    /**
     * This method sets the graph associated with this panel and renders it, applying a basic layout.
     * @param g The graph model to be rendered on the panel.
     */
    public void setGraph(Graph g){
        graph = g;
        for (Cluster c: g.getClusters()) {
            colors.put(c, RandomColorPicker.randomColor());
        }

        new ClusterRandomLayout(graph, getWidth(), getHeight()).apply();

        repaint();
    }

    public void refreshBounds(){
        Rectangle bounds = getBounds();
        relativeX = - bounds.width / 2;
        relativeY = - bounds.height / 2;
        repaint();
    }

    public void setColors(){
        for(Cluster cluster : graph.getClusters()){
            colors.put(cluster, RandomColorPicker.randomColor());
        }

        repaint();
    }
}
