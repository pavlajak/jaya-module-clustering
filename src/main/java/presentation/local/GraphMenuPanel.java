package presentation.local;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class GraphMenuPanel extends JPanel {

    public GraphMenuPanel(){
        super();
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        List<GraphMenuButton> buttons = new ArrayList<>();

        buttons.add(new GraphMenuButton(
                "Center graph",
                "C:/Users/jakub/IdeaProjects/smac/resource/icon/centerGraph.png"
                )
        );
        buttons.add(new GraphMenuButton(
                        "Add node",
                        "C:/Users/jakub/IdeaProjects/smac/resource/icon/addNode.png"
                )
        );

        for(GraphMenuButton button : buttons){
            button.setToolTipText(button.DESCRIPTION);
            this.add(button);
        }

        this.setBackground(Color.BLACK);
    }
}
