package model.code;

import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JarTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JavaParserTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;
import org.apache.bcel.classfile.ClassParser;
import org.apache.bcel.classfile.JavaClass;
import presentation.local.AppFrameMain;
import utils.Utils;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ClassTreeBuilder {

    private ClassTree ct;
    private JavaSymbolSolver symbolSolver;

    private List<File> classPath; //parts of classpath
    private File srcRoot; //root directory of java source files

    private List<File> classPathFiles; //.class and .jar files

    public ClassTreeBuilder(){
        classPath = new ArrayList<>();
        classPathFiles = new ArrayList<>();
    }

    public void setJavaSrcRoot(String s){
        srcRoot = new File(s);
    }

    public void setJavaSrcRoot(File f){
        srcRoot = f;
    }

    public ClassTree build(){
        ct = new ClassTree();

        CombinedTypeSolver cts = new CombinedTypeSolver();

        cts.add(new ReflectionTypeSolver()); //reflectively imports classes from runtime (java.* and javax.*)

        cts.add(new JavaParserTypeSolver(srcRoot)); //imports classes from java source files

        //Process Class Path
        classPathFiles = findClassPathFiles();
        //processClassPathFiles(); //adds classpath files to ClassTree
        for (File f: classPathFiles) {
            try{
                cts.add(new JarTypeSolver(f)); //imports classes from classpath jars
            } catch (IOException e){
                System.err.println(e.getCause() + " (file: " + f + ")");
            }
        }

        //creates symbol solver with reflection classes, classpath classes and source code classes
        symbolSolver = new JavaSymbolSolver(cts);

        //Process source files
        processJavaFiles();

        System.out.println("Source files processed.");

        //ct.prettyPrint();

        return ct;
    }

    private void readJavaFile(File f){
        System.out.println("Processing file " + f);
        ClassBuilder cb = new ClassBuilder();
        cb.setSymbolSolver(symbolSolver);
        cb.setJavaSrc(f);
        Class c = null;
        try{
            c = cb.build();
        } catch (StackOverflowError e){
            System.err.println("File " + f + " likely contains generics.");
        }
        if(c == null){
            System.out.println("File " + f + " contains no class.");
            return;
        }
        ct.addClassRecursive(c);
    }


    private void processJavaFiles(){
        List<File> files = findJavaFiles(srcRoot);
        for (File item: files) {
            readJavaFile(item);
        }
    }

    public static List<File> findJavaFiles(File file){
        List<File> files = new ArrayList<>();
        Utils.rFindFiles(file, files, ".java");
        return files;
    }

    private List<File> findClassPathFiles(){
        List<File> files = new ArrayList<>();
        for (File item: classPath) {
            Utils.rFindFiles(item, files, ".class");
            Utils.rFindFiles(item, files, ".jar");
        }
        return files;
    }

    private void processClassPathFiles(){
        for (File file: classPathFiles) {
            if(file.getName().endsWith(".jar")){
                readClassesFromJar(file);
            } else if (file.getName().endsWith(".class")){
                System.err.println("ClassPath included .class files (not only .jar). Some references may be incomplete. [" + file.getName() + "]");
                readClass(file);
            }
        }
    }

    public ClassTreeBuilder addClassPath(String path){
        addClassPath(new File(path));
        return this;
    }

    public ClassTreeBuilder addClassPath(File path){
        classPath.add(path);
        return this;
    }

    private void readClassesFromJar(File jar){
        try{
            JarFile jf = new JarFile(jar);
            Enumeration<JarEntry> jarEnum = jf.entries();
            while(jarEnum.hasMoreElements()){
                JarEntry je = jarEnum.nextElement();
                if(!je.getName().endsWith(".class")) continue;
                BufferedInputStream bis = new BufferedInputStream(jf.getInputStream(je));
                readClass(bis, je.getName());
            }

        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private void readClass(InputStream is, String fileName){
        try {
            ClassParser cp = new ClassParser(
                    is,
                    fileName
            );
            JavaClass jc = cp.parse();
            ct.addClassRecursive(new Class(jc.getClassName()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readClass(File classFile){
        try {
            FileInputStream fis = new FileInputStream(classFile);
            readClass(fis, classFile.getName());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
