package model.code;

import java.util.ArrayList;
import java.util.List;

public class Class {
    private String name;
    private List<ExtReference> externalCalls;
    private List<ExtReference> externalFields;

    public Class(){
        externalCalls = new ArrayList<>();
        externalFields = new ArrayList<>();
    }

    public Class(String name){
        this();
        this.name = name;
    }

    public void addExtAccess(ExtReference er){
        externalFields.add(er);
    }

    public void addExtCall(ExtReference er){
        externalCalls.add(er);
    }

    public List<ExtReference> getExternalCalls() {
        return externalCalls;
    }

    public List<ExtReference> getExternalAccessess() {
        return externalFields;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return name;
    }

    public void prettyPrint(){
        StringBuilder sb = new StringBuilder();
        sb.append(name);
        sb.append(" { extFi: ");
        externalFields.forEach(extReference -> {
            sb.append(extReference.toString());
        });
        sb.append(", extCa: ");
        externalCalls.forEach(extReference -> {
            sb.append(extReference.toString());
            sb.append(" ");
        });
        sb.append("}");
        System.out.println(sb.toString());
    }

}
