package model.code;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.*;
import com.github.javaparser.ast.expr.*;

import com.github.javaparser.printer.YamlPrinter;
import com.github.javaparser.resolution.declarations.ResolvedReferenceTypeDeclaration;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.model.resolution.TypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JavaParserTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;

import java.io.*;
import java.util.*;

public class ClassBuilder {

    private File javaSrcFile;
    private List<File> javaSrcContextFiles;
    private Class target;

    private JavaSymbolSolver symbolSolver;

    private String qualifiedName;
    private List<ExtReference> refs;

    public ClassBuilder(){
        refs = new ArrayList<>();
        javaSrcContextFiles = new ArrayList<>();
    }

    public void setJavaSrc(File src){
        javaSrcFile = src;
    }

    public Class build(){
        target = new Class();
        try{
            StaticJavaParser.getConfiguration().setSymbolResolver(symbolSolver);
            CompilationUnit cu = StaticJavaParser.parse(javaSrcFile);

            ResolvedReferenceTypeDeclaration r = cu.getPrimaryType().get().resolve();
            target.setName(r.getQualifiedName());

            //Method calls
            cu.findAll(MethodCallExpr.class).forEach(methodCallExpr -> {
                if(methodCallExpr.getScope().isPresent()) {
                    ExtReference er = new ExtReference();
                    String erTarget = methodCallExpr.getScope().get().calculateResolvedType().describe();
                    if(erTarget.equals(target.getName())) return; //cancelling reference creation in case of self-reference
                    er.setTarget(erTarget);
                    er.setRef(methodCallExpr.getName() + "()");
                    er.setDepth(translateLineage(getLineage(methodCallExpr)));
                    target.addExtCall(er);
                }
            });

            //Object creations ("new" calls)
            cu.findAll(ObjectCreationExpr.class).forEach(objectCreationExpr -> {

            });

            //Field accesses
            cu.findAll(FieldAccessExpr.class).forEach(fieldAccessExpr -> {

            });
            //target.prettyPrint();
        } catch (NoSuchElementException nsee){
            return null;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return target;
    }

    private static List<String> getLineage(Node n){
        List<String> context = new ArrayList<>();
        followLineage(n.getParentNode().get(), context);
        return context;
    }

    public static List<CodeLayer> translateLineage(List<String> s){
        List<CodeLayer> cls = new ArrayList<>();
        boolean lastStmt = false;
        String current;
        for (String value : s) {
            current = value;
            switch (current) {
                case "TryStmt":
                    cls.add(CodeLayer.TRY);
                    break;
                case "CatchClause":
                    cls.add(CodeLayer.CATCH);
                    break;
                case "IfStmt":
                    if (lastStmt) cls.add(CodeLayer.IF);
                    break;
                case "ConditionalExpr":
                    cls.add(CodeLayer.COND);
                    break;
                case "SwitchStmt":
                    cls.add(CodeLayer.SWITCH);
                    break;
            }
            lastStmt = current.contains("Stmt");
        }
        return cls;
    }

    private static void followLineage(Node n, List<String> lineage){
        lineage.add(n.getClass().getSimpleName());
        Optional<Node> parent = n.getParentNode();
        if(parent.isPresent()){
            followLineage(parent.get(), lineage);
        }
    }

    public void setSymbolSolver(JavaSymbolSolver symbolSolver) {
        this.symbolSolver = symbolSolver;
    }
}
