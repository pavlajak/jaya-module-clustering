package model.code;

import java.util.ArrayList;
import java.util.List;

public class ExtReference {
    private String target;
    private String ref;

    public List<CodeLayer> getDepth() {
        return depth;
    }

    private List<CodeLayer> depth;

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public void addDepthLayer(CodeLayer cl) {
        depth.add(cl);
    }

    public void setDepth(List<CodeLayer> cl) {
        depth = cl;
    }

    public ExtReference(){
        depth = new ArrayList<>();
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("{ ");
        sb.append(target);
        sb.append(".");
        sb.append(ref);
        sb.append(" [ ");
        depth.forEach(codeLayer -> {
            sb.append(codeLayer);
            sb.append(" ");
        });
        sb.append("] }");
        return sb.toString();
    }

}
