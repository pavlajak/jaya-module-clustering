package model.code;

import model.graph.Arc;
import model.graph.Graph;
import model.graph.Node;

import java.util.*;

public class ClassTree implements Comparable<ClassTree> {

    private String name;
    private ClassTree parent;
    private Map<String, ClassTree> children;
    private Map<String, Class> classes;

    public Graph getGraph() {
        Graph g = new Graph();
        Set<Class> cls = getAllDescendantClasses();
        cls.forEach(cl -> {
            g.addNode(new Node(cl.getName()));
        });
        cls.forEach(cl -> {
            Node n = g.getNodeByLabel(cl.getName());
            Map<String, Arc> arcs = new HashMap<>();
            List<ExtReference> ers = cl.getExternalCalls();
            ers.forEach(call -> {
                Arc tmp = arcs.get(call.getTarget());
                if (tmp == null) {
                    if(g.getNodeByLabel(call.getTarget()) != null) {
                        tmp = new Arc(
                                n,
                                g.getNodeByLabel(call.getTarget()),
                                1
                        );
                        n.addArc(tmp);
                    }
                } else {
                    tmp.setWeight(tmp.getWeight() + 1);
                }

            });
            Arc a = new Arc();
        });
        return g;
    }

    public Set<Class> getAllDescendantClasses() {
        Set<Class> ret = new HashSet<>(classes.values());
        children.forEach((k, ct) -> {
            ret.addAll(ct.getAllDescendantClasses());
        });
        return ret;
    }

    public ClassTree getClassTree(String name) {
        String[] fullName = name.split("\\.");
        ClassTree cur = this;
        for (int i = 0; i < fullName.length; i++) {
            cur = cur.getChild(fullName[i]);
            if (cur == null) break;
        }
        return cur;
    }

    public ClassTree() {
        children = new HashMap<>();
        classes = new HashMap<>();
        name = "";
    }

    public ClassTree(String name, ClassTree parent) {
        this();
        this.name = name;
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        StringBuilder sb = new StringBuilder();
        Stack<String> s = new Stack<>();
        ClassTree cur = this.parent;
        while (cur.parent != null) {
            s.push(cur.name);
            cur = cur.parent;
        }
        String toAppend;
        while (!s.isEmpty()) {
            sb.append(s.pop());
            sb.append('.');
        }
        sb.append(this.name);
        return sb.toString();
    }

    public ClassTree getChild(String key) {
        return children.get(key);
    }

    public Class getChildClass(String name) {
        return classes.get(name);
    }

    public void addClassRecursive(Class c) {
        String[] fullName = c.getName().split("\\.");
        ClassTree current = this;
        ClassTree tmp;
        for (int i = 0; i < fullName.length - 1; i++) {
            if ((tmp = current.getChild(fullName[i])) == null) {
                tmp = new ClassTree(fullName[i], current);
                current.addChild(tmp);
            }
            current = tmp;
        }
        current.addClass(c);
    }

    public void addClass(Class c) {
        classes.put(c.getName(), c);
    }

    public void addChild(ClassTree child) {
        children.put(child.name, child);
    }

    @Override
    public int compareTo(ClassTree t) {
        return name.compareTo(t.name);
    }

    public Set<ClassTree> getChildren() {
        return new HashSet<>(children.values());
    }

    public void prettyPrint() {
        prettyPrint(0);
    }

    private void prettyPrint(int depth) {
        StringBuilder sb = new StringBuilder();
        sb.append(" ".repeat(depth));
        sb.append(getShortName());
        sb.append(" { ");
        classes.forEach((k, cls) -> {
            sb.append(cls.getName());
            sb.append(" ");
        });
        sb.append("}");
        System.out.println(sb.toString());
        children.forEach((k, child) -> {
            child.prettyPrint(depth + 1);
        });
    }

    private String getShortName() {
        int lastDot = name.lastIndexOf('.');
        if (lastDot != -1) {
            return name.substring(lastDot);
        } else {
            return name;
        }
    }

    public Class findClass(String name) {
        String[] fullName = name.split("\\.");
        ClassTree current = this;
        ClassTree tmp;
        for (int i = 0; i < fullName.length - 1; i++) {
            if ((tmp = current.getChild(fullName[i])) == null) {
                return null;
            }
            current = tmp;
        }
        return current.getChildClass(fullName[fullName.length - 1]);
    }

    public Set<ClassTree> getLowestTrees() {
        LowPackageCollector lpc = new LowPackageCollector();
        accept(lpc);
        return lpc.getLowestCts();
    }

    public void accept(CTVisitor v) {
        v.visit(this);
    }

}
