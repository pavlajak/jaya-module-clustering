package model.code;

public enum CodeLayer {
    IF,
    TRY,
    CATCH,
    COND,
    SWITCH
}
