package model.code;

import java.util.HashSet;
import java.util.Set;

public class PackageCollector implements CTVisitor {

    private Set<ClassTree> cts;

    public PackageCollector(){
        cts = new HashSet<>();
    }

    @Override
    public void visit(ClassTree ct) {
        cts.add(ct);
        for(ClassTree child: ct.getChildren()){
            child.accept(this);
        }
    }

    public Set<ClassTree> getCts(){
        return cts;
    }
}
