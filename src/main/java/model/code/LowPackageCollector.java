package model.code;

import java.util.HashSet;
import java.util.Set;

public class LowPackageCollector implements CTVisitor {

    private Set<ClassTree> lowestCts;

    public LowPackageCollector(){
        lowestCts = new HashSet<>();
    }

    @Override
    public void visit(ClassTree ct) {
        if(ct.getChildren().isEmpty()){
            lowestCts.add(ct);
        } else {
            for(ClassTree child: ct.getChildren()){
                child.accept(this);
            }
        }
    }

    public Set<ClassTree> getLowestCts(){
        return lowestCts;
    }
}
