package model.alg;

import model.graph.Arc;
import model.graph.Cluster;
import model.graph.Graph;
import model.graph.Node;

import java.util.*;
import java.util.stream.Collectors;

public class MSJaya implements Clustering{

    private Graph graph;
    private Random r;

    private final double M = 10;

    public MSJaya(Graph g){
        graph = g;
        r = new Random();
    }

    private void random(){
        Set<Cluster> clusters = graph.getClusters();
        clusters.clear();

        Set<Node> nodes = new HashSet<>();
        graph.getNodes().entrySet().forEach(entry -> {
            nodes.add(entry.getValue());
        });
        int clusterCount = nodes.size() / 3;
        List<Node> nodeList = new ArrayList<>(nodes);

        List<Cluster> clusterList = new ArrayList<>();
        for (int i = 0; i < clusterCount; i++) {
            clusterList.add(new Cluster("cluster" + i));
        }

        nodeList.forEach(node -> {
            clusterList.get(r.nextInt(clusterCount)).addNode(node);
        });

        clusterList.forEach(cluster -> {
            graph.addCluster(cluster);
        });
    }

    /**
     * Maximize intra edges
     * Minimize inter edges
     * Maximize cluster count
     * Maximize MQ
     * Minimize cluster size difference
     */
    private void ECA(){
        System.out.println("Starting Jaya...");
        graph.getClusters().clear();

        List<Node> nodes = new ArrayList<>(graph.getNodeSet());

        int minClusterCount = 2;
        int maxClusterCount = nodes.size() / 2;

        int popSize = 10;
        List<Solution> pop = randomPopulation(popSize, nodes);
        List<Solution> pareto = new ArrayList<>(popSize);

        int bestIdx = 0;
        int worstIdx = bestIdx;

        int total = 0;
        int t = 1;
        int T = 64;

        while(t < T){
            int delta = r.nextInt(popSize);
            bestIdx = r.nextInt(popSize);
            int nonImprCount = 0;
            for (int i = 0; i < popSize; i++) {
                if(nonImprCount == delta){
                    t = 1;
                    nonImprCount = 0;
                }
                double alpha = M * (1 - t/T);

                for (int j = 0; j < pop.size(); j++) {
                    Solution solution = pop.get(j);
                    Solution newSol = updateSolution(solution, pop.get(bestIdx), pop.get(worstIdx), alpha, maxClusterCount);
                    if(dominates(newSol, solution, nodes)){
                        pop.set(i, newSol);
                        if(dominates(newSol, pop.get(bestIdx), nodes)){
                            bestIdx = i;
                            pareto.add(newSol);
                        }
                    } else {
                        nonImprCount++;
                        if(dominates(solution, pop.get(worstIdx), nodes)){
                            worstIdx = i;
                        }
                    }

                    Solution best = pop.get(bestIdx);
                    for (int i1 = 0; i1 < pop.size(); i1++) {
                        Solution s = pop.get(i);
                        if(s != best){
                            if(dominates(s, best, nodes)){
                                pareto.remove(best);
                            }
                        }
                    }
                    for (int i1 = 0; i1 < pop.size(); i1++) {
                        Solution s = pop.get(i);
                        if(s != best){
                            if(dominates(best, s, nodes)){
                                pareto.remove(s);
                            }
                        }
                    }
                }
            }
            t++;
            total++;
            if(total > 128){
                T *= .9;
            }
            if(total % 20 == 0) {
                System.out.println("Working...");
                //System.out.printf("t: %d, T: %d, total: %d\n", t, T, total);
            }
        }


        Solution s = null;
        double maxMQ = 0;
        for (int i = 0; i < pop.size(); i++) {
            double mq = MQ(graphFromSolution(pop.get(i), nodes));
            if(mq >= maxMQ){
                s = pop.get(i);
            }
        }
        Graph sGraph = graphFromSolution(s, nodes);
        sGraph.getClusters().forEach(cluster -> {
            graph.addCluster(cluster);
        });
        System.out.println("Done!");
    }

    public boolean dominates(Solution s1, Solution s2, List<Node> nodes){
        Graph g1 = graphFromSolution(s1, nodes);
        Graph g2 = graphFromSolution(s2, nodes);

        boolean isBetter = false;

        //max MQ
        double g1mq = MQ(g1);
        double g2mq = MQ(g2);
        if(g1mq > g2mq) {
            isBetter = true;
        } else {
            return false;
        }

        //max cluster count
        if(g1.getClusters().size() > g2.getClusters().size()) {
            isBetter = true;
        }


        //max intra edge sum
        if(intraEdgeGraphSum(g1) > intraEdgeGraphSum(g2)){
            isBetter = true;
        }

        //min iter edge sum
        if(interEdgeGraphSum(g1) < interEdgeGraphSum(g2)){
            isBetter = true;
        }


        return isBetter;
    }

    public static Graph graphFromSolution(Solution s, List<Node> nodes){
        Graph g = new Graph();
        Map<Integer, Cluster> clusters = new HashMap<>();
        for (int i = 0; i < s.nodeClusterArr.length; i++) {
            clusters.put((int) Math.round(s.nodeClusterArr[i]), new Cluster("cluster" + s.nodeClusterArr[i]));
        }
        for (int i = 0; i < s.nodeClusterArr.length; i++) {
            Node cur = nodes.get(i);
            clusters.get((int) Math.round(s.nodeClusterArr[i])).addNode(cur);
        }
        nodes.forEach(node -> {
            g.addNode(node);
        });
        clusters.forEach((integer, cluster) -> {
            g.addCluster(cluster);
        });
        return g;
    }

    public Solution updateSolution(Solution sol, Solution bestSol, Solution worstSol, double alpha, int maxCluster){
        Solution newSol = copySolution(sol);
        double r1 = r.nextDouble();
        double r2 = r.nextDouble();
        double[] nca = newSol.nodeClusterArr;
        double[] best = bestSol.nodeClusterArr;
        double[] worst = worstSol.nodeClusterArr;
        for (int i = 0; i < nca.length; i++) {
            nca[i] += alpha * (r1 * (best[i] - nca[i]) - r2 * (worst[i] - nca[i]));
            //nca[i] += (r1 * (best[i] - nca[i]) - r2 * (worst[i] - nca[i]));
        }

        double max = getMax(nca);
        double min = getMin(nca);
        double diff = max - min;
        if(min < 0){
            for (int i = 0; i < nca.length; i++) {
                nca[i] -= min;
            }
        }
        if(diff > maxCluster){
            double scale = maxCluster / diff;
            for (int i = 0; i < nca.length; i++) {
                nca[i] *= scale;
            }
        }

        return newSol;
    }

    public Solution copySolution(Solution sol){
        Solution newSol = new Solution(sol.nodeClusterArr.length);
        return newSol;
    }

    class Solution {
        public double[] nodeClusterArr;
        public double MQ;
        public Solution(int nodeCount){
            nodeClusterArr = new double[nodeCount];
        }
        public Solution(Solution s){
            Solution t = new Solution(s.nodeClusterArr.length);
            nodeClusterArr = Arrays.copyOf(s.nodeClusterArr, s.nodeClusterArr.length);
        }
    }

    public List<Solution> randomPopulation(int size, List<Node> nodes){
        List<Solution> pop = new ArrayList<>(size);
        int minClusterCount = 2;
        int maxClusterCount = nodes.size() / 2;
        for (int i = 0; i < size; i++) {
            pop.add(randomSolution(nodes));
        }
        return pop;
    }

    public Solution randomSolution(List<Node> nodes){
        Solution sol = new Solution(nodes.size());
        int minClusterCount = 2;
        int maxClusterCount = nodes.size() / 2;
        int clusterCount = r.nextInt(maxClusterCount - minClusterCount) + minClusterCount;

        //prep cluster indexes
        int[] clusterIdxs = new int[clusterCount];
        List<Integer> tmp = new ArrayList<>();
        while(tmp.size() < clusterCount){
            int next = r.nextInt(maxClusterCount);
            if(!tmp.contains(next)){
                tmp.add(next);
            }
        }
        int c = 0;
        for (Integer integer : tmp) {
            clusterIdxs[c++] = integer;
        }

        for (int i = 0; i < sol.nodeClusterArr.length; i++) {
            sol.nodeClusterArr[i] = clusterIdxs[(int) (r.nextDouble() * clusterIdxs.length)];
        }

        return sol;
    }

    @Override
    public void apply() {
        ECA();
    }

    private double MQ(Graph graph){
        double sumMQ = 0;
        for (Cluster cluster : graph.getClusters()) {
            double intraSum = intraEdgeClusterSum(cluster);
            double interSum = interEdgeClusterSum(cluster);
            if(intraSum != 0){
                sumMQ += intraSum / (intraSum + .5 * interSum);
            }
        }
        return sumMQ;
    }

    private int intraEdgeNodeSum(Node node){
        int sum = 0;
        Cluster c = node.getCluster();

        //arcs leaving cluster
        for (Arc arc : node.getArcs()) {
            if(c.containsNode(arc.getTo()) && c.containsNode(arc.getFrom())){
                sum += arc.getWeight();
            }
        }

        //arcs entering cluster
        for (Arc arc : node.getArcsTo()) {
            if(c.containsNode(arc.getTo()) && c.containsNode(arc.getFrom())){
                sum += arc.getWeight();
            }
        }
        return sum;
    }

    private int intraEdgeClusterSum(Cluster c){
        int sum = 0;
        for (Node node : c.getNodes()) {
            sum += intraEdgeNodeSum(node);
        }
        return sum;
    }

    private int intraEdgeGraphSum(Graph graph){
        int sum = 0;
        for (Cluster cluster : graph.getClusters()) {
            sum += intraEdgeClusterSum(cluster);
        }
        return sum;
    }

    private int interEdgeNodeSum(Node node){
        int sum = 0;
        Cluster c = node.getCluster();
        for (Arc arc : node.getArcs()) {
            if(!c.containsNode(arc.getTo()) || !c.containsNode(arc.getFrom())){
                sum += arc.getWeight();
            }
        }
        return sum;
    }

    private int interEdgeClusterSum(Cluster c){
        int sum = 0;
        for (Node node : c.getNodes()) {
            sum += interEdgeNodeSum(node);
        }
        return sum;
    }

    private int interEdgeGraphSum(Graph graph){
        int sum = 0;
        for (Cluster cluster : graph.getClusters()) {
            sum += interEdgeClusterSum(cluster);
        }
        return sum;
    }

    private static double getMin(double[] a){
        double min = a[0];
        for (int i = 1; i < a.length; i++) {
            if(min > a[i]){
                min = a[i];
            }
        }
        return min;
    }

    private static double getMax(double[] a){
        double max = a[0];
        for (int i = 1; i < a.length; i++) {
            if(max < a[i]){
                max = a[i];
            }
        }
        return max;
    }

}
