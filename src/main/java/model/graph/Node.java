package model.graph;

import java.io.IOException;
import java.io.Writer;
import java.util.HashSet;
import java.util.Set;

public class Node {
    private String label;
    private Set<Arc> arcs;
    private Set<Arc> arcsTo;
    private int size;
    private double xCoord;
    private double yCoord;

    public double xDispl;
    public double yDispl;

    private Cluster cluster;

    public Node(){
        arcs = new HashSet<>();
        arcsTo = new HashSet<>();
    }

    public Node(String label){
        this();
        this.label = label;
        this.size = 10;
    }

    public void addArc(Arc arc){
        arcs.add(arc);
        arc.getTo().addArcTo(arc);
    }

    public void addArcTo(Arc arc){
        arcsTo.add(arc);
    }

    public int getSize() {
        return size;
    }

    public Node(String label, int size){
        this(label);
        this.size = size;
    }

    public Node(String label, int size, Cluster c){
        this(label);
        this.size = size;
    }

    public String getLabel() {
        return label;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Set<Arc> getArcs() {
        return arcs;
    }

    public Set<Arc> getArcsTo(){return arcsTo;}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[Node: ");
        sb.append(label);
        sb.append(" (size: ");
        sb.append(size);
        sb.append(" ["); sb.append(xCoord); sb.append(", "); sb.append(yCoord); sb.append("]");
        sb.append("), arcs: ");
        arcs.forEach((e)->sb.append(e.toString()));
        sb.append("]");
        return sb.toString();
    }

    public double x() {
        return xCoord;
    }

    public void setX(double xCoord) {
        this.xCoord = xCoord;
    }

    public double y() {
        return yCoord;
    }

    public void setY(double yCoord) {
        this.yCoord = yCoord;
    }

    public Cluster getCluster() {
        return cluster;
    }

    public void setCluster(Cluster cluster) {
        this.cluster = cluster;
    }

    public void writeAsXml(Writer w) throws IOException {
        w.write("<node>");
        w.write("<label>" + label + "</label>");
        w.write("<size>" + size + "</size>");
        w.write("<arcs>");
        for (Arc arc: arcs) {
            arc.writeAsXml(w);
        }
        w.write("</arcs>");
        w.write("</node>");
    }

    public void writeAsJson(Writer w) throws IOException{
        w.write("{ \"label\": \"" + label + "\", ");
        w.write("\"size\": " + size + ", ");
        w.write("\"arcs\": [ ");

        int counter = arcs.size();
        for (Arc arc : arcs) {
            counter--;
            arc.writeAsJson(w);
            if(counter != 0) w.write(", ");
        }
        w.write(" ] }");
    }
}
