package model.graph;

import java.io.IOException;
import java.io.Writer;

public class Arc {
    private Node from, to;
    private int weight;

    public Arc(){}

    public Arc(Node from, Node to, int weight){
        this.from = from;
        this.to = to;
        this.weight = weight;
    }

    public Node getFrom() {
        return from;
    }

    public Node getTo() {
        return to;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString(){
        return "[" + from.getLabel() + "->" + to.getLabel() + "(" + weight + ")]";
    }

    public void writeAsXml(Writer w) throws IOException {
        w.write("<arc to=\"" + to.getLabel() + "\" weight=\"" + weight + "\"/>");
    }

    public void writeAsJson(Writer w) throws IOException {
        w.write("{ \"to\": \"" + to.getLabel() + "\", \"weight\" : " + weight + " }");
    }
}
