package model.graph;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import model.code.ClassTree;
import model.code.ClassTreeBuilder;
import model.code.PackageCollector;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.*;

public class Graph {

    private String name;
    private Map<String, Node> nodes;
    private Set<Cluster> clusters;

    public String getName() {
        return name;
    }

    public Map<String, Node> getNodes() {
        return nodes;
    }

    public Set<Node> getNodeSet(){
        Set<Node> nodeSet = new HashSet<>();
        getNodes().entrySet().forEach(entry -> {
            nodeSet.add(entry.getValue());
        });
        return nodeSet;
    }

    public Set<Arc> getArcs(){
        HashSet<Arc> arcs = new HashSet<>();
        Collection<Node> nodesCol = nodes.values();
        for (Node node : nodesCol) {
            for (Arc arc : node.getArcs()) {
                arcs.add(arc);
            }
        }
        return arcs;
    }

    public Set<Cluster> getClusters() {
        return clusters;
    }

    public Graph(){
        nodes = new HashMap<>();
        clusters = new HashSet<>();
    }

    public Graph(String name){
        this();
        this.name = name;
    }

    /**
     * Method that gets a node based on @label.
     * @param label of node to be retrurned.
     * @return Node with label @label.
     */
    public Node getNodeByLabel(String label){
        return nodes.get(label);
    }

    public void addNode(Node n){
        nodes.put(n.getLabel(), n);
    }

    public void addCluster(Cluster c){
        clusters.add(c);
    }

    /**
     * This method populates this graph instance from an xml file.
     * @param filepath is the absolute path to the xml file.
     * @exception IOException On input error.
     * @exception ParserConfigurationException On incorrect parser configuration.
     * @exception SAXException On malformed XML.
     */
    public void readFromXml(String filepath) throws ParserConfigurationException, IOException, SAXException {
        File f = new File(filepath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbFactory.newDocumentBuilder();
        Document doc = db.parse(f);

        doc.getDocumentElement().normalize();

        NodeList nl = doc.getDocumentElement().getElementsByTagName("node");
        int nlLength = nl.getLength();
        org.w3c.dom.Node n;

        for (int i = 0; i < nlLength; i++) {
            n = nl.item(i);
            if(n.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE){
                readFromXmlNode(n);
            }
        }

        nl = doc.getDocumentElement().getElementsByTagName("cluster");
        nlLength = nl.getLength();

        for (int i = 0; i < nlLength; i++) {
            n = nl.item(i);
            if(n.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE){
                readFromXmlCluster(n);
            }
        }
    }

    private void readFromXmlCluster(org.w3c.dom.Node node){
        Cluster c = new Cluster(node.getAttributes().getNamedItem("label").getNodeValue());
        System.out.println("adding cluster " + c);
        org.w3c.dom.Node curNode = node.getFirstChild();
        while(curNode != null){
            if (curNode.getNodeType() != org.w3c.dom.Node.ELEMENT_NODE) {
                curNode = curNode.getNextSibling();
                continue;
            }
            if(curNode.getNodeName().equals("nodeLabel")){
                c.addNode(getNodeByLabel(curNode.getTextContent()));
            }
            curNode = curNode.getNextSibling();
        }
        addCluster(c);
    }

    private void readFromXmlNode(org.w3c.dom.Node node){
        org.w3c.dom.Node curNode = node.getFirstChild();
        boolean labelSet = false, sizeSet = false, arcsSet = false;
        String nodeLabel = "missinglabel";
        int size = -1;

        org.w3c.dom.Node arcsNode = null;

        label:
        while (curNode != null) {
            if (curNode.getNodeType() != org.w3c.dom.Node.ELEMENT_NODE) {
                curNode = curNode.getNextSibling();
                continue;
            }
            switch (curNode.getNodeName()) {
                case "label":
                    nodeLabel = curNode.getTextContent();
                    labelSet = true;
                    if (sizeSet && arcsSet) break label;
                    break;
                case "size":
                    size = Integer.parseInt(curNode.getTextContent());
                    sizeSet = true;
                    if (labelSet && arcsSet) break label;
                    break;
                case "arcs":
                    arcsNode = curNode;
                    arcsSet = true;
                    if (labelSet && sizeSet) break label;
                    break;
            }
            curNode = curNode.getNextSibling();
        }

        Node n = getNodeByLabel(nodeLabel);
        if(n == null){
            n = new Node(nodeLabel, size);
        } else if(n.getSize() == -1){ //if node has been added with unknown size, add size
            n.setSize(size);
        }

        if(arcsNode == null){ //no arcs found
            return;
        }
        org.w3c.dom.Node arc = arcsNode.getFirstChild();
        Node to;
        while(arc != null){
            if(!arc.getNodeName().equalsIgnoreCase("arc")){
                arc = arc.getNextSibling();
                continue;
            }
            String toLabel = arc.getAttributes().getNamedItem("to").getTextContent();
            to = getNodeByLabel(toLabel);
            if(to == null){
                to = new Node(toLabel);
                addNode(to);
            }
            n.addArc(new Arc(n, to, Integer.parseInt(arc.getAttributes().getNamedItem("weight").getTextContent())));
            arc = arc.getNextSibling();
        }
        addNode(n);
    }

    /**
     * This method populates this graph instance from a json file.
     * @param filepath is the absolute path to the json file.
     * @exception FileNotFoundException On non existing file.
     */
    public void readFromJson(String filepath) throws FileNotFoundException {
        if(name == null){
            name = filepath;
        }

        BufferedReader bfr = new BufferedReader(new FileReader(filepath));
        JsonParser parser = new JsonParser();
        JsonElement root = parser.parse(bfr);
        JsonObject graph = root.getAsJsonObject();
        JsonArray nodes = graph.getAsJsonArray("nodes");
        if(nodes == null) return;
        for (JsonElement node : nodes) {
            readFromJsonNode(node.getAsJsonObject());
        }

        JsonArray clusters = graph.getAsJsonArray("clusters");
        if(clusters == null) return;
        for (JsonElement cluster: clusters) {
            readFromJsonCluster(cluster.getAsJsonObject());
        }
    }

    private void readFromJsonCluster(JsonObject cluster){
        System.out.println("Adding cluster:" + cluster.toString());
        String clusterLabel = cluster.get("label").getAsString();
        Cluster c = new Cluster(clusterLabel);

        JsonArray nodes = cluster.getAsJsonArray("nodes");
        JsonObject node;
        Iterator<JsonElement> it = nodes.iterator();
        while(it.hasNext()){
            node = it.next().getAsJsonObject();
            Node n = getNodeByLabel(node.get("label").getAsString());
            c.addNode(n);
        }
        addCluster(c);
    }

    private void readFromJsonNode(JsonObject node){
        //System.out.println("Adding node: " + node.toString());
        String nodeLabel = node.get("label").getAsString();
        int size = node.get("size").getAsInt();
        Node n = getNodeByLabel(nodeLabel);
        if(n == null){
            n = new Node(nodeLabel, size);
        } else if(n.getSize() == -1){ //if node has been added with unknown size, add size
            n.setSize(size);
        }

        JsonArray arcs = node.getAsJsonArray("arcs");
        Iterator<JsonElement> it = arcs.iterator();
        JsonObject arc;
        Node to;
        while(it.hasNext()){
            arc = it.next().getAsJsonObject();
            String toLabel = arc.get("to").getAsString();
            to = getNodeByLabel(toLabel);
            if(to == null){
                to = new Node(toLabel);
                addNode(to);
            }
            n.addArc(new Arc(n, to, arc.get("weight").getAsInt()));
        }
        addNode(n);
    }

    public void writeAsXml(Writer w) throws IOException {
        w.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        w.write("<graph><nodes>");
        for (Node node: nodes.values()) {
            node.writeAsXml(w);
        }
        w.write("</nodes><clusters>");
        for (Cluster cluster: clusters) {
            cluster.writeToXml(w);
        }
        w.write("</clusters></graph>");
        w.flush();
        w.close();
    }

    public void writeAsJson(Writer w) throws IOException{
        w.write("{ \"nodes\": [ ");
        int nodeCounter = nodes.size();
        for (Node node : nodes.values()) {
            nodeCounter--;
            node.writeAsJson(w);
            if(nodeCounter != 0) w.write(", ");
        }
        w.write("], \"clusters\": [ ");
        int clusterCounter = clusters.size();
        for(Cluster cluster: clusters){
            clusterCounter--;
            cluster.writeAsJson(w);
            if(clusterCounter != 0) w.write(", ");
        }
        w.write(" ] }");
        w.flush();
        w.close();
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[Graph: ");
        sb.append(name);
        sb.append(", nodes: ");
        nodes.forEach((k,v)-> sb.append(v.toString()));
        sb.append(", clusters: ");
        clusters.forEach(cluster -> sb.append(cluster.toString()));
        sb.append(" ]");
        return sb.toString();
    }


}
