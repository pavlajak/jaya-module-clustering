package model.graph;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Cluster {
    private String label;
    private Set<Node> nodes;
    private Map<String, Object> properties;

    public Cluster(String l){
        label = l;
        nodes = new HashSet<>();
        properties = new HashMap<>();
    }

    public void addNode(Node n){
        nodes.add(n);
        n.setCluster(this);
    }

    public String getLabel() {
        return label;
    }

    public Set<Node> getNodes() {
        return nodes;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    public void addProperty(String name, Object property){
        properties.put(name, property);
    }

    public boolean containsNode(Node n){
        return nodes.contains(n);
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[Cluster: " );
        sb.append(label);
        sb.append(", nodes: ");
        nodes.forEach(node -> sb.append("[" + node.getLabel() + "]"));
        sb.append("]");
        return sb.toString();
    }

    public void writeToXml(Writer w) throws IOException {
        w.write("<cluster label=\"" + label + "\">");
        for (Node node: nodes) {
            w.write("<nodeLabel>" + node.getLabel() + "</nodeLabel>");
        }
        w.write("</cluster>");
    }

    public void writeAsJson(Writer w) throws IOException {
        w.write("{ \"label\": \"" + label + "\", \"nodes\": [ ");

        int counter = nodes.size();
        for (Node node: nodes) {
            counter--;
            w.write("{\"label\": \"" + node.getLabel() + "\"}");
            if(counter != 0) w.write(", ");
        }
        w.write(" ] } ");
    }
}
