package utils;

import java.io.File;
import java.util.List;

public class Utils {
    public static void rFindFiles(File file, List<File> files, String endsWith){
        if(file == null) return;
        for (File item : file.listFiles() ) {
            if(item.isFile() && item.getName().endsWith(endsWith)){
                files.add(item);
            } else if (item.isDirectory()){
                rFindFiles(item, files, endsWith);
            }
        }
    }
}
