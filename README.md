# Jaya Module Clustering

With code analysis as its main goal, this project uses the Jaya algorithm to solve a module clustering problem in Java source code.

## Setup

You need to be able to use `java` in your terminal. Installation instruction can be found for example [here](https://docs.oracle.com/en/java/javase/14/install/overview-jdk-installation.html).

> Please note that you can only use the code scanning feature of this program on projects that use the same or older JDK versions.

At this time it is not possible to use a different JDK for reference resolution - 
this means that if your code references `java.util.List` it references the class that 
is part of the JRE you use to run this program.

You need to have Maven installed. You can find instructions at the [official maven site](https://maven.apache.org/install.html).

## Build and run

The recommended way of using this project is to build it using Maven and package it into a `JAR`. The produced `JAR` is executable. You can do this by running 
```
mvn package
```

You can also run `mvn test` to run tests or run `mvn compile` to compile the resources.

After compiling you can run the program from the command line using
```
java -jar jmc.jar <arguments>
```

If run JMC without arguments it will display this help text

```
JMC usage options (choose one):
        -h | -help                                                      print usage options
        -g                                                              launch the gui version
        -r <root>  -c <classpath-to-scan> -xml|json <save-to-file>      scan a java project, analyze and save
        -j <graph-file> -xml|json <save-to-file>                        run Jaya clustering on .json or .xml graph file
Suggested usage: either launch the gui using '-g' or
[1] run '-f root -c classpath-to-scan -xml|json file'
[2] run '-jaya file' -xml|json file'
```

## Known Issues

At this moment there is an issue with the code scanner which causes a `StackOverflow` if you scan a source file that uses generic types.
This is reported as a defect of JavaParser. A workaround for JMC is that ignores classes with generic types.
